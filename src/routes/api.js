
const express = require('express');

const UsersController = require('../controllers/api/userControllers');

function routes() {
    const router = express.Router();

    router.get('/user', function(req, res){
        UsersController.index(req, res);
    });

    router.post('/user', function(req, res){
        UsersController.create(req, res);
    });

    router.put('/user/:userID', function(req, res){
        UsersController.update(req, res);
    });

    router.delete('/user/:userID', function(req, res){
        UsersController.remove(req, res);
    });

    router.get('/user/:userID', function(req, res){
        UsersController.getByUserID(req,res);
    });

    return router;

}

module.exports = routes;
