import SessionModel from '../models/SessionModel';
import UserModel from '../models/UserModel';

import { v1 } from 'uuid';
class Session {

    static generateSessionID() {
        return v1();
    }

    static getAppCookies(req, key) {
        if (!req.headers.cookie) {
            return false;
        }
        // We extract the raw cookies from the request headers
        const rawCookies = req.headers.cookie.split('; ');
        // rawCookies = ['myapp=secretcookie, 'analytics_cookie=beacon;']

        const parsedCookies = {};
        rawCookies.forEach(rawCookie => {
            const parsedCookie = rawCookie.split('=');
            // parsedCookie = ['myapp', 'secretcookie'], ['analytics_cookie', 'beacon']
            parsedCookies[parsedCookie[0]] = parsedCookie[1];
        });
        return parsedCookies[key];
    };

    static hasSession(req) {
        const sessionId = this.getAppCookies(req, 'user');
        if (sessionId) {
            req.session = sessionId;
            return true;
        }
        return false;
    }

    static async getUser(req) {
        try {
            const sessionModel = SessionModel();
            const userModel = UserModel();

            userModel.hasOne(sessionModel, { foreignKey: 'UserID', as: 'Session' });

            const sessionData = await userModel.findOne({
                include: [
                    {
                        model: sessionModel,
                        as: 'Session',
                        where: {
                            SessionID: req.session
                        }
                    }
                ]
            });

            return {
                ID: sessionData.ID,
                Email: sessionData.Email,
                Username: sessionData.Username,
                FullName: sessionData.FullName,
            };
        } catch (error) {
            throw new Error(error);
        }
    }

    // make session here
    static async makeSession(res, UserID) {
        try {
            const sessionModel = SessionModel();
            const sessionData = await sessionModel.create({
                UserID,
                SessionID: v1(),
            });
            res.cookie('user', sessionData.SessionID, {
                httpOnly: true,
                // secure : true:
                secure: process.env.NODE_ENV === 'development' ? false : true,
                //maxAge :6000; => batas waktu login dalam detik
            });
        } catch (error) {
            throw new Error(error);
        }
    }

    static async clearSession(req, res) {
        try {
            const { ID } = req.user;
            const sessionModel = SessionModel();
            await sessionModel.destroy({
                where: {
                    UserID: ID,
                }
            });
            res.clearCookie('user');
        } catch (error) {
            throw new Error(error);
        }
    }


}

export class SessionFlash {
    constructor() {
        this._flashMessage = {
            message: '',
            type: '',
        };
    }

    static flashMessage(res, { message = '', type = '' }) {

        res.cookie('flashMessage', message, {
            httpOnly: true,
            secure: false,
            maxAge: 1200,
        });
        res.cookie('flashMessageType', type, {
            httpOnly: true,
            secure: false,
            maxAge: 1200,
        });
    }

    static getFlashMessage(req) {
        const flashMessage = Session.getAppCookies(req, 'flashMessage');
        const flashMessageType = Session.getAppCookies(req, 'flashMessageType');
        if (flashMessage && flashMessageType) {
            return {
                message: flashMessage.split('%20').join(' ').trim(),
                type: flashMessageType,
            };
        }
        return {
            message: '',
            type: ''
        };
    }

    static clearFlashMessage(req, res) {
        const flashMessage = Session.getAppCookies(req, 'flashMessage');
        const flashMessageType = Session.getAppCookies(req, 'flashMessageType');
        if (flashMessage && flashMessageType) {
            res.clearCookie('flashMessage');
            res.clearCookie('flashMessageType');
        }

        // this.flashMessage(res, { message: '', type: '' });
    }
}

export default Session;
