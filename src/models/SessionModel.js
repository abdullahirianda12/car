import { DataTypes } from 'sequelize';
import { Database } from '../libraries/database';

function Session() {
    // .define('namaPerwakilanTabel', {} -> kolom tabel di database, {} -> konfigurasi / mapping ke tabel apa di Postgre )
    return Database.define('Session', {
        SessionID: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
        },
        UserID: {
            type: DataTypes.NUMBER,
            allowNull: false,
        },
    }, {
        tableName: 'tb_session',
        timestamps: false, // 
    });
}

export default Session;
