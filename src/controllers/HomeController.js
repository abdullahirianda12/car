
class HomeController {
    index(req, res) {
      res.render("pages/home");
    }
    account(req, res) {
      res.render("pages/account");
    }
  }
  
  module.exports = new HomeController();
  